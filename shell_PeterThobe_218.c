/*
 * shell_PeterThobe_218.c
 *
 *  Created on: Feb 24, 2018
 *      Author: thobe
 */

#include "stdio.h"
#include "string.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

#define MAXLINE 80 // the max size of a line of input

int main(void)
{
	char args[MAXLINE/2+1] ; //String to retrieve user argument input
	int shouldRun = 1 ; //Control variable for the program 1= run/0 = do not run
	char* ptr[MAXLINE/2+1]; //array of char ptr to tokenize user provided args
	int waitCtrl = 0;//ctrl variable to force parent to wait for child to exit 1 = not wait/0 = wait
	pid_t pid;

	printf("CS149 Shell from Peter Thobe:(type:quit to exit program)\n");
	//control loop that will repeat while shouldRun == 1
	while(shouldRun == 1)
	{
		//fflush(stdout);
		printf("Thobe-218> ");
		fflush(stdin);
		fgets(args, 100, stdin);
		//control loop
		if(strcmp(args,"quit\n") == 0)
		{
			shouldRun = 0;
		}
		else
		{

			//checking if parent should wait for child process
			if(args[strlen(args) - 2] == '&')
			{
				waitCtrl = 1 ;	//dont wait
				args[strlen(args) - 2] = '\n';
			}
			else
			{
				waitCtrl = 0 ;
			}

			//
			int index = 0;

			do
			{
				if(index == 0)
				{
					ptr[index] = strtok(args, " \n");
					index++ ;
				}
				else
				{
					ptr[index] = strtok(NULL, " \n") ;
					index++;
				}
			}while(ptr[index - 1] != NULL);

			fflush(stdout); //flushing the std output


			//Process based coding below

			pid = fork();

			if(pid < 0)//error occurec during fork();
			{
				printf("fork error.\n");
			}
			else if(pid == 0)//child process
			{
				execvp(ptr[0], ptr);
					printf("error in execvp(): incorrect argument passed\n");// only called if exec fails
					_exit(0);
			}
			else if (pid > 0)//parent process
			{
				if(waitCtrl == 0)
				{
					wait(NULL);
				}
			}

			//end of else code
		}

	}

	printf("Program exiting\n");

	return 0;
}



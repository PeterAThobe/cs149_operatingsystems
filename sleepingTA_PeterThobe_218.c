/*
 * sleepingTA_PeterThobe_218.c
 *
 *  Created on: Mar 28, 2018
 *      Author: thobe
 */

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h> //pthread library
#include <semaphore.h> //semephore library
#include <unistd.h>
//Constans
#define MAX_SLEEP_TIME 3 // Maximum sleep time of a thread
#define NUM_OF_STUDENTS 4 // Number of potential students
#define NUM_OF_HELPS 2	// Number of helps each student can recieve
#define NUM_OF_SEATS 2	// Number of waiting seats available
#define TRUE 1
#include <time.h> //may need to remove this

//Posix Synchronization(Global Variables)

pthread_mutex_t mutex_lock	; // Mutex Variable. protect the global variable waiting_students

sem_t students_sem	;
sem_t ta_sem	;

int waiting_students = 0	;// the number of waiting students







typedef struct
{
	pthread_t tid;
	int numHelps ;
	int seed;
}studentParameters;

typedef struct
{
	pthread_t tid;
	int seed;
}taParameter;


//main program
int main(void)
{
	printf("CS149 SleepingTA from Peter Thobe.\n");
	/*
	 * Parameters for the Student Threads
	 * total threads = 5
	 */

	studentParameters* studentParams[NUM_OF_STUDENTS]	;
	taParameter* taParam;

	//Student and TA Threads

	pthread_t studentThreads[NUM_OF_STUDENTS] ;

	pthread_t taThread ;

	//Malloc params
	for(int i = 0 ; i < NUM_OF_STUDENTS  ; i++)
	{
		studentParams[i] = (studentParameters*)malloc(sizeof(studentParameters)) ;
	}

	for(int i = 0 ; i < NUM_OF_STUDENTS ; i++)
	{
		studentParams[i]->tid = i + 1 ;
		studentParams[i]->numHelps = 0 ;
		studentParams[i]->seed = i * 5 ;
	}
	taParam = (taParameter*)malloc(sizeof(taParameter));
	taParam->seed = 100;
	taParam->tid = 100;

	//Initialize Mutex and Semaphores
		sem_init(&ta_sem,0,0) ; //look at what i should initialize these to
		sem_init(&students_sem,0,0) ;

		//mutex init
		pthread_mutex_init(&mutex_lock,NULL) ;

	//end of Mutex and Semaphore initializing

	//Student Threads Function
	void* student(void* arg)
	{
		studentParameters* student = (studentParameters*) arg ;

		unsigned int seed = student->seed ;
		unsigned int rand = 0 ;
		do
		{
			//default action to do some programming
			rand = (rand_r(&seed) % MAX_SLEEP_TIME) + 1 ;
			printf("\tStudent %d programming for %d seconds.\n",(int)student->tid,(int)rand);
			student->numHelps++ ;
			sleep(rand);

			pthread_mutex_lock(&mutex_lock);

			if(waiting_students < NUM_OF_SEATS)
			{

					++waiting_students;
					pthread_mutex_unlock(&mutex_lock) ;
					printf("\t\t\tStudent %d takes a seat, # of waiting Students = %d\n",
											(int)student->tid,waiting_students);
					sem_post(&students_sem);
					student->numHelps++ ;
				sem_wait(&ta_sem) ;
					printf("\t\tStudent %d recieving help\n",(int)student->tid);



			}
			else
			{
				pthread_mutex_unlock(&mutex_lock);
				printf("\t\t\t\tstudent %u wil try again Later!\n", student->tid);
			}

		}while(student->numHelps <= NUM_OF_HELPS);

		pthread_exit(0);
	}


	//TA Threads Function
	void* TA(void* arg)
		{
			taParameter* param = (taParameter*)arg ;
			unsigned int rand = 0;
			unsigned int seed =param->seed ;


				while(TRUE)
				{
					rand  = rand_r(&seed)%MAX_SLEEP_TIME +1 ;
					pthread_mutex_lock(&mutex_lock);//get lock

					if(waiting_students > 0)
					{
						sem_wait(&students_sem);
							printf("helping student for %u second(s), # waiting students = %d\n"
																		,rand, --waiting_students) ;

							pthread_mutex_unlock(&mutex_lock);//release lock
							sleep(rand) ;
							sem_post(&ta_sem);
					}
					else
					{
						pthread_mutex_unlock(&mutex_lock);//release lock
						sleep(rand);
					}

				}

		}


	//threads creation

	for(int i = 0 ; i < NUM_OF_STUDENTS ; i++)
	{
		pthread_create(&studentThreads[i],NULL, student , (void*)studentParams[i]) ;
	}

	pthread_create(&taThread,NULL,TA,(void *)taParam) ;



	//Wait for student threads then destroy TA thread
	for(int i = 0 ; i < NUM_OF_STUDENTS ; i++)
	{
		pthread_join(studentThreads[i],NULL) ;
	}

	pthread_cancel(taThread) ;


	// free memory of threads structures
		for(int i = 0 ; i < NUM_OF_STUDENTS ; i++)
		{
			free(studentParams[i]) ;
		}

		free(taParam);

	// Delete Mutex and Semaphores below
		sem_destroy(&students_sem);
		sem_destroy(&ta_sem);

		pthread_mutex_destroy(&mutex_lock);
	//Exit Main

	printf("Exiting Program!\n");
	return 0	;
}



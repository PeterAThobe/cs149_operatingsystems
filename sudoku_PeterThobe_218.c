/*
 * sudoku_PeterThobe_218.c
 *
 *  Created on: Mar 2, 2018
 *      Author: thobe
 */

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#define BOARD_SIZE 9 //used to define the sudoku board size
#define NUM_THREADS 27

typedef struct
{
	int row;
	int col;
	int checkArray[BOARD_SIZE];
}param;

int main(void)
{
	param *parameters[NUM_THREADS] ;

	int returns[NUM_THREADS] ;
	int success = 1;//0 = failed / 1 = Valid
	for(int i = 0 ; i < NUM_THREADS ; i++)
	{
		parameters[i] = (param*) malloc(sizeof(param));
	}

	for(int i = 0 ; i < BOARD_SIZE ; i++)
	{
		//setting the row and col params
		parameters[i]->row = i ;
		parameters[i] -> col = 0 ;
		parameters[i+BOARD_SIZE] -> row = 0 ;
		parameters[i+BOARD_SIZE] -> col = i ;
//		functions below set the top left corner of the array for the sub arrays
		parameters[i + (2 * BOARD_SIZE)] -> row = (i/3)*3 ;
		parameters[i + (2 * BOARD_SIZE)] -> col = (i%3)*3 ;

		for(int j = 0 ; j < BOARD_SIZE ; j++)
		{
			parameters[i]-> checkArray[j] = j+1 ;
			parameters[i+BOARD_SIZE]-> checkArray[j] = j+1 ;
			parameters[i + (2 * BOARD_SIZE)]-> checkArray[j] = j+1;
		}

	}

	printf("CS149 Sudoku from Peter Thobe: \n");

	//Hard Coded test game board
	int sudokuBoard [BOARD_SIZE][BOARD_SIZE] =
	{
			{6,5,3,1,2,8,7,9,4},
			{1,7,4,3,5,9,6,8,2},
			{9,2,8,4,6,7,5,3,1},
			{2,8,6,5,1,4,3,7,9},
			{3,9,1,7,8,2,4,5,6},
			{5,4,7,6,9,3,2,1,8},
			{8,6,5,2,3,1,9,4,7},
			{4,1,2,9,7,5,8,6,3},
			{7,3,9,8,4,6,1,2,5}
	};



	//Threads
	pthread_t threads[NUM_THREADS];

	//functions to use
	void* checkRow(void* arg)
	{
		param *args = (param*)arg;
		int row = args->row;
		int valid = 1 ;


		for(int i = 0 ; i < BOARD_SIZE ; i++) // controls iteration through SudokuBoard values
		{
			for (int j = 0 ; j < BOARD_SIZE ; j++)
			{
				if(sudokuBoard[row][i] == args->checkArray[j])
				{
					args->checkArray[j] = 0;
				}
			}
		}
		for(int i = 0 ; i < BOARD_SIZE ; i++)
		{
			if(args->checkArray[i]  != 0)
			{
				valid = 0 ;
			}
		}


		free(arg);

		pthread_exit(valid);
	}

	void* checkColumn(void* arg)
		{
			param *args = (param*)arg;
			int col = args->col;
			int valid = 1 ;


			for(int i = 0 ; i < BOARD_SIZE ; i++) // controls iteration through SudokuBoard values
			{
				for (int j = 0 ; j < BOARD_SIZE ; j++)
				{
					if(sudokuBoard[i][col] == args->checkArray[j])
					{
						args->checkArray[j] = 0;
					}
				}
			}
			for(int i = 0 ; i < BOARD_SIZE ; i++)
			{
				if(args->checkArray[i]  != 0)
				{
					valid = 0 ;
				}
			}


			free(arg);

			pthread_exit(valid);
		}

	void* checkSubArray(void* arg)
	{
		param *args = (param*)arg;
		int row = args->row;
		int col = args->col;
		int valid = 1 ;


		for(int i = row ; i < row + 3 ; i++) // controls iteration through SudokuBoard rows
		{
			for(int j = col ; j < col + 3 ; j++) // controls iteration through sb columns
			{
				for(int k = 0 ; k < BOARD_SIZE ; k++)
				{
					if(sudokuBoard[i][j] == args->checkArray[k])
					{
						args->checkArray[k] = 0;
					}
				}
			}
		}
		for(int i = 0 ; i < BOARD_SIZE ; i++)
		{
			if(args->checkArray[i]  != 0)
			{
				valid = 0 ;
			}
		}

		free(arg);

		pthread_exit(valid);
	}

	//Thread Creation for Parallel runnning
	for(int i = 0 ; i < NUM_THREADS ; i++)
	{
		if(i < 9)
		{
			pthread_create(&threads[i],NULL,checkRow,(void *)parameters[i]);
		}
		else if(i < 18)
		{
			pthread_create(&threads[i],NULL,checkColumn,(void *)parameters[i]);
		}
		else
		{
			pthread_create(&threads[i],NULL,checkSubArray,(void *)parameters[i]);
		}
	}
	//Main threading wait for loop for worker Threads to finish
	for(int i = 0 ; i< NUM_THREADS ; i++)
	{
		pthread_join(threads[i],&returns[i]);
	}

	for(int i = 0 ; i < NUM_THREADS ; i++)
	{
		if(returns[i] != 1)
		{
			success = 0;
		}
	}

	for(int i = 0 ; i < BOARD_SIZE ; i++)
	{
		printf("{ ");
		for(int j = 0 ; j < BOARD_SIZE ; j++)
		{
			printf("%d ",sudokuBoard[i][j]);
		}
		printf("}\n");
	}

	if(success)
	{
		printf("Sudoku puzzle: Valid Entry!\n");
	}
	else
	{
		printf("Sudoku puzzle: Invalid Entry!\n");
	}


	return 0; //return from the program
}



